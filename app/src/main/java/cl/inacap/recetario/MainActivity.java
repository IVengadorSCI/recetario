package cl.inacap.recetario;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    TextView tvNombreReceta, tvIngredientes;
    EditText etReceta;
    Button btBuscar;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.tvNombreReceta = (TextView) findViewById(R.id.nombre_receta);
        this.tvIngredientes = (TextView) findViewById(R.id.ingredientes);

        this.etReceta = (EditText) findViewById(R.id.etReceta);
        this.btBuscar = (Button) findViewById(R.id.btBuscar);

        tvIngredientes.setMovementMethod(new ScrollingMovementMethod());

        btBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String url = "https://api.edamam.com/search?q="+etReceta.getText()+"&app_id=ecfd0b03&app_key=a6254d708dc672d3c769011fdf6a15b7";

                StringRequest solicitud = new StringRequest(
                        Request.Method.GET,
                        url,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                // Tenemos respuesta desde el servidor
                                try {
                                    JSONObject jsonRespuesta = new JSONObject(response);

                                    JSONArray jsonHits = jsonRespuesta.getJSONArray("hits");

                                    JSONObject jsonRecipes = jsonHits.getJSONObject(0);
                                    JSONObject jsonRecipe = jsonRecipes.getJSONObject("recipe");
                                    String platillo = jsonRecipe.getString("label");

                                    JSONArray jsonIngredients = jsonRecipe.getJSONArray("ingredients");

                                    int contador = jsonIngredients.length();

                                    String ingrediente = "";

                                    for (int i = 0; i < contador ; i++){
                                        JSONObject jsonIngredient = jsonIngredients.getJSONObject(i);
                                        ingrediente += "*" + jsonIngredient.getString("text") + " \n ";
                                    }


                                    int calorias =  jsonRecipe.getInt("calories");

                                    JSONObject jsonTotalNutrients = jsonRecipe.getJSONObject("totalNutrients");
                                    JSONObject jsonSugar = jsonTotalNutrients.getJSONObject("SUGAR");

                                    String azucar = "Azucar: " + jsonSugar.getInt("quantity") + " " + jsonSugar.getString("unit");

                                    JSONObject jsonSodio = jsonTotalNutrients.getJSONObject("NA");

                                    String sodio = "Sodio: " + jsonSodio.getInt("quantity") + " " + jsonSodio.getString("unit");

                                    tvNombreReceta.setText("Receta de "+ platillo);
                                    tvIngredientes.setText("Ingredientes: \n"+ ingrediente + "\nDatos Nutricionales \nCalorias: " + calorias + " klac \n" + azucar + "\n"+ sodio);



                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // Algo fallo
                            }
                        }
                );

                RequestQueue listaEspera = Volley.newRequestQueue(getApplicationContext());
                listaEspera.add(solicitud);
            }
        });









    }
}
